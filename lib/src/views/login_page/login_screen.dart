import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:salles_tools/src/bloc/login_bloc/login_bloc.dart';
import 'package:salles_tools/src/bloc/login_bloc/login_event.dart';
import 'package:salles_tools/src/bloc/login_bloc/login_state.dart';
import 'package:salles_tools/src/bloc/register_bloc/register_bloc.dart';
import 'package:salles_tools/src/services/login_service.dart';
import 'package:salles_tools/src/utils/hex_converter.dart';
import 'package:salles_tools/src/utils/shared_preferences_helper.dart';
import 'package:salles_tools/src/views/bottom_navigation.dart';
import 'package:salles_tools/src/views/components/loading_content.dart';
import 'package:salles_tools/src/views/components/log.dart';
import 'package:salles_tools/src/views/login_page/verification_contact.dart';
import 'package:salles_tools/src/views/register_page/register_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  var usernameCtrl = new TextEditingController();
  var passwordCtrl = new TextEditingController();

  void _onNavVerification() {
    Navigator.of(context).push(
        PageRouteBuilder(
          pageBuilder: (_, __, ___) => VerificationContactView(),
          transitionDuration: Duration(milliseconds: 300),
          transitionsBuilder:
              (_, Animation<double> animation, __, Widget child) {
                return SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1, 0),
                    end: Offset.zero,
                  ).animate(animation),
                  child: child,
                );
          },
        ),
    );
  }

  void _onNavDashboard() {
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (_, __, ___) => BottomNavigationDrawer(),
        transitionDuration: Duration(milliseconds: 300),
        transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          );
        },
      ),
    );
  }

  void onNavigator() async {
    var firstInstall = await SharedPreferencesHelper.getFirstInstall();

    if (firstInstall != null) {
      Navigator.of(context).pop();
      _onNavVerification();
    }

    if (firstInstall == "no") {
      Navigator.of(context).pop();
      _onNavDashboard();
    }
  }

  void _onRegister() {
    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (_, __, ___) => BlocProvider<RegisterBloc>(
          create: (context) => RegisterBloc(LoginService()),
          child: RegisterScreen(),
        ),
        transitionDuration: Duration(milliseconds: 750),
        transitionsBuilder:
            (_, Animation<double> animation, __, Widget child) {
          return Opacity(
            opacity: animation.value,
            child: child,
          );
        },
      ),
    );
  }

  void _onSubmit() {
    if (_formKey.currentState.validate()) {
      // ignore: close_sinks
      final loginBloc = BlocProvider.of<LoginBloc>(context);
      loginBloc.add(FetchLogin(usernameCtrl.text, passwordCtrl.text));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: BlocListener<LoginBloc, LoginState>(
        listener: (context, state) {
          if (state is LoginSuccess) {
            Navigator.of(context).pop();
            _onNavVerification();
          }

          if (state is LoginFailed) {
            Navigator.of(context).pop();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text("Error"),
                backgroundColor: HexColor('#C61818'),
              ),
            );
          }

          if (state is LoginError) {
            Navigator.of(context).pop();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text("${state.error.errorDescription}"),
                backgroundColor: HexColor('#C61818'),
              ),
            );
          }

          if (state is LoginLoading) {
            onLoading(context);
          }
        },
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: 300,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 150, right: 20),
                      child: Image.asset(
                        "assets/icons/old_hasjrat_toyota_logo.png",
                        height: 180,
                        width: 230,
                      ),
                    ),
                  ),
                  Positioned(
                    top: -30,
                    left: -30,
                    child: Container(
                      height: 170,
                      width: 170,
                      decoration: BoxDecoration(
                        color: HexColor('#212120'),
                        borderRadius: BorderRadius.circular(200),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 80,
                    left: -50,
                    child: Container(
                      height: 130,
                      width: 130,
                      decoration: BoxDecoration(
                        color: HexColor('#C61818'),
                        borderRadius: BorderRadius.circular(200),
                      ),
                    ),
                  ),
                ],
              ),
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Login",
                            style: TextStyle(
                              letterSpacing: 1.0,
                              fontSize: 20,
                              color: Colors.grey,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: 'Username',
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: HexColor('#C61818'),
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'NIK wajib diisi';
                              }
                              return null;
                            },
                            controller: usernameCtrl,
                          ),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'Password',
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: HexColor('#C61818'),
                                ),
                              ),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Password wajib diisi';
                              }
                              return null;
                            },
                            controller: passwordCtrl,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 25, horizontal: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Forgot Password?",
                                  style: TextStyle(
                                    letterSpacing: 1.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        _onSubmit();
                      },
                      color: HexColor('#C61818'),
                      child: Text(
                        "Login",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ],
                ),
              ),
//              Padding(
//                padding: const EdgeInsets.only(top: 25),
//                child: Row(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    Text(
//                      "New User ?",
//                      style: TextStyle(
//                        letterSpacing: 0.5,
//                      ),
//                    ),
//                    FlatButton(
//                      onPressed: () {
////                        _onRegister();
//                      },
//                      child: Text(
//                        "Registration",
//                        style: TextStyle(
//                          fontWeight: FontWeight.w700,
//                          letterSpacing: 1.0,
//                          color: HexColor('#C61818'),
//                          decoration: TextDecoration.underline,
//                        ),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
            ],
          ),
        ),
      ),
    );
  }
}
